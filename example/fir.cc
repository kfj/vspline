/************************************************************************/
/*                                                                      */
/*    vspline - a set of generic tools for creation and evaluation      */
/*              of uniform b-splines                                    */
/*                                                                      */
/*            Copyright 2015 - 2022 by Kay F. Jahnke                    */
/*                                                                      */
/*    Permission is hereby granted, free of charge, to any person       */
/*    obtaining a copy of this software and associated documentation    */
/*    files (the "Software"), to deal in the Software without           */
/*    restriction, including without limitation the rights to use,      */
/*    copy, modify, merge, publish, distribute, sublicense, and/or      */
/*    sell copies of the Software, and to permit persons to whom the    */
/*    Software is furnished to do so, subject to the following          */
/*    conditions:                                                       */
/*                                                                      */
/*    The above copyright notice and this permission notice shall be    */
/*    included in all copies or substantial portions of the             */
/*    Software.                                                         */
/*                                                                      */
/*    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND    */
/*    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES   */
/*    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND          */
/*    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT       */
/*    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,      */
/*    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING      */
/*    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR     */
/*    OTHER DEALINGS IN THE SOFTWARE.                                   */
/*                                                                      */
/************************************************************************/

/// \file fir.cc
///
/// \brief  apply an FIR filter to an image
///
/// vspline has some code which isn't useful only for b-splines.
/// One example is the application of a FIR filter to a MultiArrayView.
/// With vspline's multithreaded SIMD code, this is done efficiently.
/// This example program will apply a 1D kernel along the horizontal
/// and vertical. You can pass an arbitrarily long sequence of filter
/// coefficients after an image file name.
///
/// compile with:
/// ./examples.sh fir.cc
///
/// invoke passing an image file and the filter's coefficients. the result
/// will be written to 'fir.tif'

#include <iostream>
#include <stdlib.h>

#include <vspline/general_filter.h>

#include <vigra/stdimage.hxx>
#include <vigra/imageinfo.hxx>
#include <vigra/impex.hxx>

// we silently assume we have a colour image

typedef vigra::RGBValue < float , 0 , 1 , 2 > pixel_type; 

// target_type is a 2D array of pixels  

typedef vigra::MultiArray < 2 , pixel_type > target_type ;

int main ( int argc , char * argv[] )
{
  if ( argc < 2 )
  {
    std::cerr << "pass a colour image file as argument," << std::endl ;
    std::cerr << "followed by the filter's coefficients" << std::endl ;
    exit( -1 ) ;
  }

  // get the image file name
  
  vigra::ImageImportInfo imageInfo ( argv[1] ) ;

  std::vector < vspline::xlf_type > kernel ;
  char * end ;

  for ( int i = 2 ; i < argc ; i++ )
    kernel.push_back ( vspline::xlf_type ( strtold ( argv [ i ] , &end ) ) ) ;

  // create an array for the image data
  
  target_type image ( imageInfo.shape() ) ;
  
  // load the image data
                   
  vigra::importImage ( imageInfo , image ) ;
  
  // apply the filter

  vspline::convolution_filter
   ( image ,
     image ,
     { vspline::MIRROR , vspline::MIRROR } ,
     kernel ,
     kernel.size() / 2 ) ;

  // store the result with vigra impex

  vigra::ImageExportInfo eximageInfo ( "fir.tif" );
  
  std::cout << "storing the target image as 'fir.tif'" << std::endl ;
  
  vigra::exportImage ( image ,
                       eximageInfo
                       .setPixelType("UINT8") ) ;
  
  exit ( 0 ) ;
}
