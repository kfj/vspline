#! /bin/bash

# compile all examples both with clang++ and with g++, with all
# SIMD backends. This will produce eight binaries for every example,
# where the prefix indicates the SIMD backend, and the suffix
# indicates the compiler. Previously I used -Ofast for all examples,
# but at times this is too aggressive and produces small differences
# which don't occur with -O3, so I stick with -O3 now.
# note that highway may need additional flags beyond -march=native
# to fully exploit a given CPU. Also note that -march=native is
# useful for intel/AMD CPUs but may not work on other ISAs, where you
# may need to substitute it with other compiler flags to say 'compile
# for this CPU's ISA'.
# For highway and AVX2, add -mavx2 -march=haswell -mpclmul -maes

for f in $@
do
  body=$(basename $f .cc)

  for compiler in clang++ g++
  do
  
    common_flags="-O3 -std=c++11 -march=native -pthread"

    # compile without explicit SIMD code

    echo $compiler $common_flags -ovs_${body}_$compiler $f -lvigraimpex
    $compiler $common_flags -ovs_${body}_$compiler $f -lvigraimpex

    # compile with Vc

    echo $compiler -DUSE_VC $common_flags -ovc_${body}_$compiler $f -lVc -lvigraimpex
    $compiler -DUSE_VC $common_flags -ovc_${body}_$compiler $f -lVc -lvigraimpex

    # compile with highway

    echo $compiler -DUSE_HWY $common_flags -ohwy_${body}_$compiler $f -lhwy -lvigraimpex
    $compiler -DUSE_HWY $common_flags -ohwy_${body}_$compiler $f -lhwy -lvigraimpex

    # compile with std::simd (needs std:simd implementation)

    common_flags="-O3 -std=c++17 -march=native -pthread"

    echo $compiler -DUSE_STDSIMD $common_flags -ostds_${body}_$compiler $f -lvigraimpex
    $compiler -DUSE_STDSIMD $common_flags -ostds_${body}_$compiler $f -lvigraimpex

  done

done
