/************************************************************************/
/*                                                                      */
/*    vspline - a set of generic tools for creation and evaluation      */
/*              of uniform b-splines                                    */
/*                                                                      */
/*            Copyright 2015 - 2023 by Kay F. Jahnke                    */
/*                                                                      */
/*    Permission is hereby granted, free of charge, to any person       */
/*    obtaining a copy of this software and associated documentation    */
/*    files (the "Software"), to deal in the Software without           */
/*    restriction, including without limitation the rights to use,      */
/*    copy, modify, merge, publish, distribute, sublicense, and/or      */
/*    sell copies of the Software, and to permit persons to whom the    */
/*    Software is furnished to do so, subject to the following          */
/*    conditions:                                                       */
/*                                                                      */
/*    The above copyright notice and this permission notice shall be    */
/*    included in all copies or substantial portions of the             */
/*    Software.                                                         */
/*                                                                      */
/*    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND    */
/*    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES   */
/*    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND          */
/*    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT       */
/*    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,      */
/*    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING      */
/*    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR     */
/*    OTHER DEALINGS IN THE SOFTWARE.                                   */
/*                                                                      */
/************************************************************************/

/// \file impulse_response.cc
///
/// \brief get the impulse response of a b-spline prefilter
/// 
/// filter a unit pulse with a b-spline prefilter of a given degree
/// and display the central section of the result
///
/// compile with:
/// clang++ -std=c++11 -o impulse_response -O3 -pthread impulse_response.cc
///
/// to get the central section with values beyond +/- 0.0042 of a degree 5 b-spline:
///
/// impulse_response 5 .0042
///
/// producing this output:
/// 
/// long double ir_5[] = {
/// -0.0084918610197410 ,
/// +0.0197222540252632 ,
/// -0.0458040841925519 ,
/// +0.1063780046433000 ,
/// -0.2470419274022756 ,
/// +0.5733258709616592 ,
/// -1.3217294729875093 ,
/// +2.8421709220216247 ,
/// -1.3217294729875098 ,
/// +0.5733258709616593 ,
/// -0.2470419274022757 ,
/// +0.1063780046433000 ,
/// -0.0458040841925519 ,
/// +0.0197222540252632 ,
/// -0.0084918610197410 ,
///  } ;
///
/// which, when used as a convolution kernel, will have the same effect on a signal
/// as applying the recursive filter itself, but with lessened precision due to windowing.

#include <iostream>
#include <iomanip>
#include <assert.h>
#include <vspline/multithread.h>
#include <vspline/vspline.h>

int main ( int argc , char * argv[] )
{
  if ( argc < 3 )
  {
    std::cerr << "please pass spline degree and cutoff on the command line"
              << std::endl ;
    exit ( -1 ) ;
  }

  int degree = std::atoi ( argv[1] ) ;
  
  assert ( degree >= 0 && degree <= vspline_constants::max_degree ) ;
  
  long double cutoff = std::atof ( argv[2] ) ;
  
  std::cout << "calculating impulse response with spline degree "
            << degree << " and cutoff " << cutoff << std::endl ;
            
  // using the highest-level access to prefiltering, we code:

  vspline::bspline < long double , 1 > bsp ( 1001 , degree ) ;
  
  auto v1 = bsp.core ;
  v1 [ 500 ] = 1.0 ;

  bsp.prefilter() ;

  std::cout << "long double ir_" << degree << "[] = {" << std::endl ;

  std::cout << std::fixed << std::showpoint
            << std::setprecision(std::numeric_limits<long double>::max_digits10) ;

  for ( int k = 0 ; k < 1001 ; k++ )
  {
    if ( std::abs ( v1[k] ) > cutoff )
    {
      std::cout << v1[k] << "L ," << std::endl ;
    }
  }
  std::cout << "} ;" << std::endl ;
}
